# Minds Istio Control Plane

This repository contains the Helm chart for deploying the Istio service mesh to the Minds infrastructure.

## Local Development

You should first install:

- [istioctl](https://istio.io/latest/docs/setup/getting-started/#download)
- [Helm](https://helm.sh/docs/intro/install/)
- [k3d](https://k3d.io)
- [skaffold](https://skaffold.dev/docs/install/)

You can use the `k3d.yaml` config file to create a local k3d cluster with:

```
k3d create cluster -c k3d.yaml
```

Next, initialize the Istio operator with:

```
istioctl operator init
```

Finally, deploy the chart in development mode with:

```
skaffold dev
```

## Production Deployment

The cluster will first need to have the Istio Operator deployed. You can create it with:

```
istioctl operator init
```

***TODO***: We should package the Istio Operator Helm chart and add it as a dependency for this chart.

After the operator has been deployed, you can deploy this chart:

```
helm upgrade --install \
  --create-namespace=true \
  --namespace=istio-system \
  istio .
```
